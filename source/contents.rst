.. pvimage documentation master file, created by
   sphinx-quickstart on Wed Apr  1 12:43:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pvimage's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`



.. automodule:: process
   :members:

.. automodule:: pipelines
   :members: